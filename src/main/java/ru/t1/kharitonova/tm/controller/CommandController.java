package ru.t1.kharitonova.tm.controller;

import ru.t1.kharitonova.tm.api.service.ICommandService;
import ru.t1.kharitonova.tm.api.controller.ICommandController;
import ru.t1.kharitonova.tm.model.Command;
import ru.t1.kharitonova.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final String maximumMemory = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maximumMemory);
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usageMemory));
    }

    @Override
    public void showWelcome(){
        System.out.println("** WELCOME TO TASK MANAGER**");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasiya Kharitonova");
        System.out.println("e-mail: akharitonova@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.9.0");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMAND]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported");
    }

}
