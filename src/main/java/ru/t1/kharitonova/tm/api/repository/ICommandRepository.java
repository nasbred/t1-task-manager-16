package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
