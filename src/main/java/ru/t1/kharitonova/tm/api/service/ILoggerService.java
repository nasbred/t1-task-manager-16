package ru.t1.kharitonova.tm.api.service;

public interface ILoggerService {

    Thread info(String message);

    void debug(String message);

    void command(String message);

    void error(Exception e);

}
